﻿using CefSharp;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Media.Animation;
using WebBrowserApp.Properties;
using System.Timers;
using WebBrowserApp.Model;
using System.Diagnostics;

namespace WebBrowserApp.View
{
    /// <summary>
    /// Interaction logic for MainPanelView.xaml
    /// </summary>
    public partial class MainPanelView : Window
    {
        public MainPanelView()
        {
            InitializeComponent();
            SetWebBrowserContextMenu();
        }

        private void SetWebBrowserContextMenu()
        {
            MainBrowser.MenuHandler=new SimpleHandler();
        }

        private async void MainBrowser_LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            await Task.Factory.StartNew(()=> {
                //indicates the webpage is still loading or not. while still loading circle progress is rotating
                //while webpage loaded, circle progress is stop rotating.
                if(e.IsLoading)
                {
                    Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => {
                        AnimationOnLoadingState();
                        PrLoadState.IsActive=true;
                    }));
                } else
                {
                    Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => {
                        AnimationOnLoadedState();
                        PrLoadState.IsActive=false;
                        TimerToHomePage();
                    }));
                }
            });
        }

        private void TimerToHomePage()
        {
            var delay = Task.Run(async () => {
                Stopwatch sw = Stopwatch.StartNew();
                await Task.Delay((int)Settings.Default.Timer.TotalMilliseconds);
                Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => {
                    MainBrowser.Address=Settings.Default.Url;
                }));
                sw.Stop();
            });
        }

        private void AnimationOnLoadingState()
        {
            var sb = new Storyboard();
            var animationOne = new DoubleAnimation {
                Duration=TimeSpan.FromSeconds(0.2),
                From=-50,
                To=0
            };
            Storyboard.SetTargetProperty(animationOne, new PropertyPath("(Canvas.Top)"));
            sb.Children.Add(animationOne);
            sb.Begin(GdNavigation);
            var animationTwo = new DoubleAnimation {
                Duration=TimeSpan.FromSeconds(0.2),
                From=4,
                To=55
            };
            Storyboard.SetTargetProperty(animationTwo, new PropertyPath("(Canvas.Top)"));
            sb.Children.Add(animationTwo);
            //sb.Begin(GdBrowser);
        }

        private void AnimationOnLoadedState()
        {
            var sb = new Storyboard();
            var animationOne = new DoubleAnimation {
                Duration=TimeSpan.FromSeconds(0.2),
                From=0,
                To=-50
            };
            Storyboard.SetTargetProperty(animationOne, new PropertyPath("(Canvas.Top)"));
            sb.Children.Add(animationOne);
            sb.Begin(GdNavigation);
            var animationTwo = new DoubleAnimation {
                Duration=TimeSpan.FromSeconds(0.2),
                From=55,
                To=4
            };
            Storyboard.SetTargetProperty(animationTwo, new PropertyPath("(Canvas.Top)"));
            sb.Children.Add(animationTwo);
            //sb.Begin(GdBrowser);
        }

        private void MainBrowser_LoadError(object sender, LoadErrorEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => {
                if(e.ErrorCode.ToString()=="ConnectionTimeOut"||e.ErrorCode.ToString()=="NameNotResolved")
                {
                    var result = MessageBox.Show("Koneksi terputus.Cek koneksi internet anda!\nIngin refresh page?", "Peringatan", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if(result==MessageBoxResult.Yes)
                    {
                        MainBrowser.Reload();
                    }
                }
            }));   
        }
    }
}
