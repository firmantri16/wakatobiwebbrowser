﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WebBrowserApp.Properties;

namespace WebBrowserApp.ViewModel
{
    public class ConfigurationViewModel : ViewModelBase
    {
        private string _url, _timer;

        public RelayCommand SaveCommand { get; set; }

        public ConfigurationViewModel() {
            SaveCommand = new RelayCommand(()=>DoSave());
            _url=WebBrowserApp.Properties.Settings.Default.Url;
            _timer=WebBrowserApp.Properties.Settings.Default.Timer.ToString();
        }

        public string Url {
            get => _url;
            set => Set(ref _url, value);
        }

        public string Timer{
            get => _timer;
            set => Set(ref _timer, value);
        }

        private void DoSave()
        {
            if (string.IsNullOrEmpty(Url) || Timer == "__:__:__") {
                MessageBox.Show("Isi semua kolom textbox terlebih dahulu.");
            } else {
                var setTime = TimeSpan.Parse(Timer);
                if(setTime.TotalMilliseconds < 300000)
                {
                    MessageBox.Show("Minimal timer adalah 5 menit (00:05:00)\nSilahkan isi timer kembali.","Peringatan", MessageBoxButton.OK, MessageBoxImage.Warning);
                } else
                {
                    Settings.Default.Url=_url;
                    Settings.Default.Timer = TimeSpan.Parse(_timer);
                    Settings.Default.Save();
                    //if(Settings.Default.IsFirst==true)
                    //{
                    //    Settings.Default.IsFirst=false;
                    //}

                    var result = MessageBox.Show("Konfigurasi baru telah disimpan! \nAplikasi akan direstart untuk menerapkan perubahan.", "Sukses", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    if(result==MessageBoxResult.OK)
                    {
                        Application.Current.Shutdown();
                        System.Windows.Forms.Application.Restart();
                    }
                }
            }
        }
    }
}