﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight;
using System.Windows.Forms;
using WebBrowserApp.Properties;
using WebBrowserApp.View;
using CefSharp.Wpf;
using CefSharp;

namespace WebBrowserApp.ViewModel
{
    public class MainPanelViewModel : ViewModelBase
    {
        private string _geturi;
        private int _getprogress;

        public RelayCommand<object> BackwardCommand { get; set; }
        public RelayCommand<object> ForwardCommand { get; set; }
        public RelayCommand<object> ReloadCommand { get; set; }
        public RelayCommand<object> HomeCommand { get; set; }
        public RelayCommand ConfigCommand { get; set; }
        public RelayCommand<object> AutoReloadCommand { get; set; }
        public RelayCommand CloseCommand { get; set; }
        public RelayCommand ForceCloseCommand { get; set; }
        
        public MainPanelViewModel()
        {
            HomeCommand = new RelayCommand<object>(DoHome);
            ReloadCommand = new RelayCommand<object>(DoReload);
            BackwardCommand = new RelayCommand<object>(DoBackward);
            ForwardCommand = new RelayCommand<object>(DoForward);
            ConfigCommand = new RelayCommand(()=>DoShowConfiguration());
            CloseCommand = new RelayCommand(() => DoClose());
            ForceCloseCommand = new RelayCommand(()=>DoForceClose());

            //GetCurrentApplication();
        }

        //Change webbrowser version from registry
        //private void GetCurrentApplication()
        //{
        //    int BrowserVer, RegVal;

        //    using (var Wb = new WebBrowser())
        //    {
        //        BrowserVer = Wb.Version.Major;

        //        if (BrowserVer >= 11)
        //        {
        //            RegVal = 11001;
        //        }
        //        else if (BrowserVer == 10)
        //        {
        //            RegVal = 10001;
        //        }
        //        else if (BrowserVer == 9)
        //        {
        //            RegVal = 9999;
        //        }
        //        else if (BrowserVer == 8)
        //        {
        //            RegVal = 8888;
        //        }
        //        else
        //        {
        //            RegVal = 7000;
        //        }

        //        using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", RegistryKeyPermissionCheck.ReadWriteSubTree))
        //        {
        //            if (Key.GetValue(System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe") == null)
        //                Key.SetValue(System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe", RegVal, RegistryValueKind.DWord);
        //        }
        //    }
        //}

        public string GetUri {
            get => _geturi;
            set => Set(ref _geturi, value);
        }

        public int GetProgress {
            get => _getprogress;
            set => Set(ref _getprogress, value);
        }

        private void DoForceClose()
        {
            MessageBox.Show("I'm sorry, you cannot force this app to close!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void DoShowConfiguration()
        {
            new ConfigurationView().ShowDialog();
        }

        private void DoClose()
        {
            System.Windows.Application.Current.Shutdown();
        }
        
        private void DoForward(object obj)
        {
            var browser = obj as ChromiumWebBrowser;
            if(browser.CanGoForward)
            {
                browser.Forward();
            }
        }

        private void DoBackward(object obj)
        {
            var browser = obj as ChromiumWebBrowser;
            if(browser.CanGoBack)
            {
                browser.Back();
            }
        }

        private void DoReload(object obj)
        {
            var browser = obj as ChromiumWebBrowser;
            browser.Reload();
        }

        private void DoHome(object obj)
        {
            var browser = obj as ChromiumWebBrowser;
            browser.Address=Settings.Default.Url;
        }
    }
}
